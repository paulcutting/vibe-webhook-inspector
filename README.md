# Atlassian Connect Webhook Inspector

This is a simple [atlassian-connect-express](https://bitbucket.org/atlassian/atlassian-connect-express) based Atlassian Connect add-on that allows you to inspect the response bodies of the available [webhooks in Atlassian Connect](https://developer.atlassian.com/static/connect/docs/latest/modules/common/webhook.html).

## Usage

This add-on runs on [Node.js](http://nodejs.org/). If you don't have it, install it. Then, clone this repo:

    git clone https://bitbucket.org/atlassianlabs/webhook-inspector.git

Then, install all of the dependencies:

    npm install

Then, run it against your Atlassian environment:

    node app.js

This app actually doesn't have a UI. All of the action is in the terminal. When events fire in the Atlassian app, the event and the JSON body will be logged in your console:

![Webhook Inspector](http://f.cl.ly/items/3m2r1J103u2J0Z2w2M3f/Screen%20Shot%202013-05-15%20at%203.11.42%20PM.png)