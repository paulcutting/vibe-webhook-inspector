module.exports = function (sequalize, DataTypes) {

    return sequalize.define('confluence', {
        action: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                len: [1, 250]
            }
        },
        user: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                len: [1, 250]
            }
        },
        spaceKey: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                len: [1, 250]
            }
        },
        pageId: {
            type: DataTypes.INTEGER
        },
        pageTitle: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                len: [1, 250]
            }
        },
        pageCreationDate: {
            type: DataTypes.INTEGER
        }
    });
}